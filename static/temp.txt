
var counter = 0;
$(document).ready(function () {
    $("#normal").click(function () {
        $("#welcome").css({ "color": "black" });
        $("#navbar-main").css({ "color": "black" });
        $("#navbar-profile").css({ "color": "black" });
        $("#navbar-book").css({ "color": "black" });
        $("#light").css({ "color": "black" });
        $("#darkness").css({ "color": "black" });
        $('#BODY').css({ "background-color": "white" });
    });

    $("#dark").click(function () {
        $("#welcome").css({ "color": "white" });
        $("#navbar-main").css({ "color": "white" });
        $("#navbar-profile").css({ "color": "white" });
        $("#navbar-book").css({ "color": "white" });
        $("#light").css({ "color": "white" });
        $("#darkness").css({ "color": "white" });
        $('#BODY').css({ "background-color": "black" });
    });
});

var acc = document.getElementsByClassName("accordion");
var k;

for (k = 0; k < acc.length; k++) {
    acc[k].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    });
}

setTimeout(function() {
    $(".pre-page").fadeOut("slow");
}, 500);

function starCounter(star_id) {
    var search = document.getElementById(star_id);
    if($(event.target).hasClass("fav-active")) {
        $(search).attr('src',"https://image.flaticon.com/icons/svg/149/149222.svg");
        counter--;
    } else {
        $(search).attr('src',"https://image.flaticon.com/icons/svg/291/291205.svg");
        counter++;
    }
    $(event.target).toggleClass("fav-active");
    $("#count").html(counter);
}

var i;
$.ajax({
    url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
    success: function (result) {
        for (i = 0; i < result.items.length; i++) {
            $('.book-list').append(
                '<tr>\
                    <th scope="row">'+ (i+1) +'</th>\
                    <td><img src="' + result.items[i].volumeInfo.imageLinks.thumbnail + '"></td>\
                    <td>' + result.items[i].volumeInfo.publisher + '</td>\
                    <td>' + result.items[i].volumeInfo.title +'</td>\
                    <td>' + result.items[i].volumeInfo.authors +'</td>\
                    <td>' + result.items[i].volumeInfo.description +'</td>\
                    <td> <input id="favorite' + i + '" type="image" onClick="starCounter(this.id)" src="https://image.flaticon.com/icons/svg/149/149222.svg"/> </td>\
                </tr>'
            )
        }
    }
})

function search_bar() {
    var word = $('input[name="pencarian"]').val()
    searching(word);
}

function searching(word) {
    var end = $("input").val();
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + end,
        datatype: 'json',
        success: function (word) {
            $('.book-list').empty();
            for (i = 0; i < word.items.length; i++) {
                $('.book-list').append(
                    '<tr>\
                        <th scope="row">'+ (i+1) +'</th>\
                        <td><img src="' + word.items[i].volumeInfo.imageLinks.thumbnail + '"></td>\
                        <td>' + word.items[i].volumeInfo.publisher + '</td>\
                        <td>' + word.items[i].volumeInfo.title +'</td>\
                        <td>' + word.items[i].volumeInfo.authors +'</td>\
                        <td>' + word.items[i].volumeInfo.description +'</td>\
                        <td> <input id="favorite' + i + '" type="image" onClick="starCounter(this.id)" src="https://image.flaticon.com/icons/svg/149/149222.svg"/> </td>\
                    </tr>'
                )
            }
        }
    })
}

$(document).ready(function () {
    $("#email").change(function() {
        let email =  $('#email').val();

        $.ajax({
            url: "/validate?email=" + email,
            success: function (result) {
                if(result["unique"] === false) {
                    $("#submit_button").prop("disabled", true);
                    $("#email").addClass("is-invalid");
                } else {
                    $("#submit_button").prop("disabled", false);
                    $("#email").removeClass("is-invalid");
                }
            }
        });
    });
});