from django.db import models
from django.utils import timezone
from datetime import datetime, date
# # Create your models here

class Schedule_mod(models.Model):
    Time = models.DateTimeField(default = timezone.now)
    Day = models.CharField(max_length = 10, null = True)
    Activity = models.CharField(max_length = 100, null = True)
    Details = models.CharField(max_length = 10000, null = True)

