from django import forms
from .models import Schedule_mod
from django.forms import ModelForm

class Schedule_Form(forms.ModelForm):
    class Meta:
        model = Schedule_mod
        fields = ['Time', 'Day', 'Activity', 'Details']
        widget = {
            'Time' : forms.DateTimeInput(attrs={'class': 'form-control'}),
            'Day' : forms.TextInput(attrs={'class':'form-control'}),
            'Activity' : forms.TextInput(attrs={'class':'form-control'}),
            'Details' : forms.TextInput(attrs={'class':'form-control'}),
        }