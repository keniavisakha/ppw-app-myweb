from django.shortcuts import render
from .forms import Schedule_Form
from .models import Schedule_mod
from django.http import HttpResponseRedirect
from django.urls import reverse


# Create your views here.
def homepage(request):
    return render(request, 'ppw_webapp/home.html')

def Act(request): 
    response = {'form' :Schedule_Form, 'events' : Schedule_mod}
    events = Schedule_mod
    form = Schedule_Form() 
    
    if request.method == "POST":
        # form = Schedule_Form(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('Act'))
    else:  
        return render(request, 'Schedule.html', response)

def delete(request):
    Schedule_mod.delete
    return HttpResponseRedirect(reverse('Schedule.html'))
