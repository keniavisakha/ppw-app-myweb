from django.db import models

# Create your models here.
class Status(models.Model):
    status_content = models.CharField(max_length = 100)
    id = models.AutoField(primary_key=True)

# class Signup(models.Model):
#     Name = models.CharField(max_length = 100)
#     Email = models.EmailField(max_length = 100)
#     Password = models.CharField(max_length = 100)

class Signup(models.Model):
    firstname = models.CharField(max_length = 50)
    lastname = models.CharField(max_length = 50)
    email = models.CharField(max_length = 50, unique=True)
    password = models.CharField(max_length = 50)
