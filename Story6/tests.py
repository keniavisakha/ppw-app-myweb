from django.test import TestCase, Client
from django.urls import resolve
from .models import Status

# Create your tests here.
class Story6_test(TestCase):
    def test_Story6_is_exsist(self):
        response = Client().get('/Story6/')
        self.assertEqual(response.status_code, 200)
    
    def test_Story6_status_template(self):
        response = Client().get('/Story6')
        self.assertTemplateUsed(response, 'Status.html')
    
    def test_Story6_using_index_funct(self):
        found = resolve('/Story6/')
        self.assertEqual(found.func, Story6_test)
    
    # def test_model_can_create_new_status(self):
    #     #check status content
    #     new_status = Status.objects.create(status_content = 'Hello')

    #     #Counting the status
    #     counting_all_available_status = Status.object.all().count()
    #     self.assertEqual(counting_all_available_status, 1)

        

