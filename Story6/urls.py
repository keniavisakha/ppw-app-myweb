from django.urls import include, path
from django.contrib import admin
from .import views

urlpatterns = [
    path('', views.profile, name='profile'),
    path('addstatus/', views.status, name='addstatus'),
    # path('deleteEach/<int:id_name>', views.deleteEach, name='deleteEach' ),
    path('status/',  views.index, name='index'),
    path('books/',  views.books, name='books'),    
    path('signup/', views.signup, name='signup'),
]