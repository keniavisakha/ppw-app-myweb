from django import forms

class Status_form(forms.Form):
    Status = forms.CharField(required = True, widget = forms.TextInput(attrs={'class':'form-control'}))

class SignUp_form(forms.Form):
    Name = forms.CharField(required = True, widget = forms.TextInput(attrs={'class':'form-control'}))
    Email = forms.EmailField(required = True, widget = forms.EmailInput(attrs={'class':'form-control'}))
    Password = forms.CharField(required = True, widget = forms.PasswordInput(attrs={'class':'form-control'}))
