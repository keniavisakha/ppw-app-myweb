from django.shortcuts import render
from django.db import IntegrityError
from datetime import datetime
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import Status, Signup
from .forms import Status_form, SignUp_form
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout
import json



# Create your views here.
def profile(request):
        return render(request, 'Story6/Profile.html')


def index(request):
    response = {'form' : Status_form, "Statuses" : Status}
    return render(request, 'Story6/Status.html', response)


def status(request):
    if request.method == "POST":
        form = Status_form(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            status = Status(status_content = cleaned_data['status_content'])
            status.save()
            return HttpResponseRedirect ('/')
        else:
            return HttpResponseRedirect('/')

def books(request):
        user = request.user

        if user is not None:
                user_session = request.session.get('user_session', 'private')
                request.session['user_session'] = 'private'
                return render(request, 'Story6/books.html')

        else:
                del request.session['user_session']      
                return render(request, 'Story6/Books.html')

def signup(request):
        return render(request, 'Story6/Signup.html')


# def deleteEach(request, id_name):
#     Status.objects.get(id=id_name).delete()
#     return HttpResponseRedirect('/')



# @csrf_exempt
# def emailValidation(request):
#         currentemail = request.POST.get('email', None)
#         validation = {
#                 'isExist' : Registration.objects.filter(email= currentemail).exists()
#         }
#         return JsonResponse(validation)